import Banner from "../components/Banner";
import HighLights from "../components/HighLights"



export default function Home(){
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		destination: "/Courses",
		label: "Enroll"
	}


	return(
		<>
			<Banner data={data}/>
	        <HighLights/>
		</>
	)
}