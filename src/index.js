import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavbar from "./AppNavbar";


// Importing the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*
  The syntax used in React.js is JSX
  JSX - JavaScript + XML
    - We are able to create HTML elements using JavaScript

*/


// const name = "John Smith";


// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// }

// const element= <h2>Hello, {formatName(user)}</h2>
// root.render(element);
// root,render() - allows to render/display our reactjs elements in our HTML file.


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

